import { Component, OnInit } from '@angular/core';
import { ApiManagerService } from './../api-manager.service';

@Component({
  selector: 'app-faqs',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css']
})
export class AnnouncementsComponent implements OnInit {

  	loading: boolean = true;
	data: any = null;
  error = null;
  announcements:any=null

	constructor(private api: ApiManagerService) { }

	ngOnInit() {

    this.getData();
    this.getAnnouncement();
	}

  toAnnouncement(){
    document.getElementById("menu-area").scrollIntoView({behavior:"smooth"})
      }

	getData(){
		let obs = this.api.getWebData();
		obs.subscribe((response) => {

			this.loading = false;
			this.data = response;

		}, error =>{
			this.loading = false;
			this.data = null;
			this.error = error;
		});
  }


//get announcement

getAnnouncement(){
  let obs = this.api.getAnnouncements();
  obs.subscribe((response) => {

    this.loading = false;
    this.announcements = response;
    console.log("announcement",this.announcements)

  }, error =>{
    this.loading = false;
    this.announcements = null;
    this.error = error;
  });
}


}
