import { Component, OnInit } from '@angular/core';
import { ApiManagerService } from './../api-manager.service';
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  loading: boolean = true;
  data: any = null;
  error = null;
  servicesCcompany:any=null;
  constructor(private api: ApiManagerService) { }

  ngOnInit() {

    this.services();
    this.toService();
  }

  toService(){
    document.getElementById("menu-area").scrollIntoView({behavior:"smooth"})
      }

  services(){
		let obs = this.api.getService();
		obs.subscribe((response) => {

      this.loading = true;

      this.servicesCcompany = response;
      if(this.servicesCcompany){
        this.loading = false;
        console.log("services",this.servicesCcompany)
      }


		}, error =>{
			this.loading = false;
			this.servicesCcompany = null;
			this.error = error;
		});
  }

}
