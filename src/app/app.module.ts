import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WebComponent } from './web/web.component';
import { ErrorComponent } from './error/error.component';
import { ApiManagerService } from './api-manager.service';
import { HttpClientModule } from '@angular/common/http';
import { AnnouncementsComponent } from './announcements/announcements.component';
import { SlickModule } from 'ngx-slick';
import { AgmCoreModule } from '@agm/core';
import { ServicesComponent } from './services/services.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    WebComponent,
    ErrorComponent,
    AnnouncementsComponent,
    ServicesComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDdj4Nf06-cyXdCj0yv8b60JZaEWEMby6k',
      libraries: ['places']

    }),
    AppRoutingModule,
    HttpClientModule,
    SlickModule.forRoot()
  ],
  providers: [ApiManagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
