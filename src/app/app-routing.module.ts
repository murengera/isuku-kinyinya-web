import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorComponent } from './error/error.component';
import { WebComponent } from './web/web.component';
import { AnnouncementsComponent } from './announcements/announcements.component';
import {AboutComponent} from './about/about.component';
import {ServicesComponent}from './services/services.component'


const routes: Routes = [
	{path: '', component:WebComponent},
  {path: 'announcements', component:AnnouncementsComponent},
  {path: 'about', component:AboutComponent},
  {path: 'service', component:ServicesComponent},

	{path:'**', component:ErrorComponent}
];

@NgModule({
   imports: [
    RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
