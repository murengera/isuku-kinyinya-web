import { Component, OnInit } from '@angular/core';
import { ApiManagerService } from './../api-manager.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  loading: boolean = true;
  data: any = null;
  error = null;
  modal_data: any = {};
  users:any=[];
  teams:any=[];
  statu:any="INACTIVE";
  missions:any=[];
  values:any=[];
  visions:any=[];
  companyProfile:any=[];
  constructor(private api: ApiManagerService) { }

  ngOnInit() {
    this.getData();
    this.team();
    this.toAbout();
    this.getMission();
    this.getProfileCompany();
    this.getValues();
    this.getVisons();

  }
  toAbout(){
    document.getElementById("menu-area").scrollIntoView({behavior:"smooth"})
      }


      getMission(){
        this.loading = true;
        let obs = this.api.getMissions();
        obs.subscribe((response) => {

          this.loading = false;
          this.missions = response;
          console.log("mission",this.missions)

        }, error =>{
          this.loading = false;
          this.missions = null;
          this.error = error;
        });
      }
      getVisons(){
        this.loading = true;
        let obs = this.api.getvisions();
        obs.subscribe((response) => {

          this.loading = false;
          this.visions = response;
          console.log("visions",this.visions)

        }, error =>{
          this.loading = false;
          this.visions = null;
          this.error = error;
        });
      }
      getValues(){
        this.loading = true;
        let obs = this.api.getValues();
        obs.subscribe((response) => {

          this.loading = false;
          this.values = response;
          console.log("values",this.values)

        }, error =>{
          this.loading = false;
          this.values = null;
          this.error = error;
        });
      }
      getProfileCompany(){
        this.loading = true;
        let obs = this.api.getCompanyProfile();
        obs.subscribe((response) => {

          this.loading = false;
          this.companyProfile = response;
          console.log("company profile",this.companyProfile)

        }, error =>{
          this.loading = false;
          this.companyProfile = null;
          this.error = error;
        });
      }


  getData(){
    this.loading = true;
		let obs = this.api.getWebData();
		obs.subscribe((response) => {

			this.loading = false;
			this.data = response;

		}, error =>{
			this.loading = false;
			this.data = null;
			this.error = error;
		});
  }
  team(){
    this.loading = true;
		let obs = this.api.getTeam();
		obs.subscribe((response) => {

			this.loading = false;
      this.teams = response;
      this.teams.forEach(user => {
        if ((user.phone_number) !== '+250781920501') {
          this.users.push(user)
        }

      });
      console.log("staff",this.teams)

		}, error =>{
			this.loading = false;
			this.teams = null;
			this.error = error;
		});
  }


}
