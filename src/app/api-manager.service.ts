import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import{catchError,tap,map,delay} from 'rxjs/operators'


@Injectable({
	providedIn: 'root'
})
export class ApiManagerService {

  api_root: string = "https://hellomed-api.herokuapp.com";

   api_rootWeb="https://isuku-staging-api.herokuapp.com/api/"
	localStorage = window.localStorage;

	constructor(private http: HttpClient) { }


	getWebData(){
		return this.http.get(this.api_root + "/web/data/");

  }
  getAnnouncements(){
		return this.http.get(this.api_rootWeb + "announcements/");

  }
  getMissions(){
    return this.http.get(this.api_rootWeb + "missions/");
  }
  getvisions(){
    return this.http.get(this.api_rootWeb + "visions/");
  }
  getValues(){
    return this.http.get(this.api_rootWeb + "values/");
  }
  getService(){
    return this.http.get(this.api_rootWeb + "services/");
  }
  getCompanyProfile(){
    return this.http.get(this.api_rootWeb + "companys/");
  }
  getGallery(){
    return this.http.get(this.api_rootWeb + "gallerys/");
  }
  getTeam(){
    return this.http.get(this.api_rootWeb + "users/").pipe(
      delay(8000)
    );
;
  }

}
